#!/bin/sh

rm -r ~/LIVE_BOOT

mkdir -p ~/LIVE_BOOT/chroot

cp chroot_prepare_image.sh LIVE_BOOT/chroot/

cd ~/LIVE_BOOT

debootstrap --arch=amd64  --variant=minbase  buster  ~/LIVE_BOOT/chroot  http://ftp.cz.debian.org/debian/

chroot ~/LIVE_BOOT/chroot/ ./chroot_prepare_image.sh

mkdir -p $HOME/LIVE_BOOT/scratch
mkdir -p $HOME/LIVE_BOOT/image/live

rm $HOME/LIVE_BOOT/image/live/filesystem.squashfs
sudo mksquashfs \
  $HOME/LIVE_BOOT/chroot \
  $HOME/LIVE_BOOT/image/live/filesystem.squashfs \
  -e boot \
  -noappend \

cp $HOME/LIVE_BOOT/chroot/boot/vmlinuz-*    $HOME/LIVE_BOOT/image/vmlinuz && \
cp $HOME/LIVE_BOOT/chroot/boot/initrd.img-* $HOME/LIVE_BOOT/image/initrd

sshpass -p "abcdef" \
  scp $HOME/LIVE_BOOT/image/vmlinuz \
      $HOME/LIVE_BOOT/image/live/filesystem.squashfs \
      root@10.0.0.88:/var/www/http/debian-custom/

sshpass -p "abcdef" \
  scp $HOME/LIVE_BOOT/image/initrd \
      root@10.0.0.88:/var/www/http/debian-custom/initrd.img

exit

cat <<'EOF' >$HOME/LIVE_BOOT/scratch/grub.cfg

search --set=root --file /DEBIAN_CUSTOM

insmod all_video

set default="0"
set timeout=5

menuentry "Debian Custom" {
  linux /vmlinuz boot=live quiet nomodeset
  initrd /initrd
}
EOF

touch $HOME/LIVE_BOOT/image/DEBIAN_CUSTOM

grub-mkstandalone \
  --format=x86_64-efi \
  --output=$HOME/LIVE_BOOT/scratch/bootx64.efi \
  --locales="" \
  --fonts="" \
  "boot/grub/grub.cfg=$HOME/LIVE_BOOT/scratch/grub.cfg"

(cd $HOME/LIVE_BOOT/scratch && \
  dd if=/dev/zero of=efiboot.img bs=1M count=10 && \
  mkfs.vfat efiboot.img && \
  mmd -i efiboot.img efi efi/boot && \
  mcopy -i efiboot.img ./bootx64.efi ::efi/boot/
)

grub-mkstandalone \
  --format=i386-pc \
  --output=$HOME/LIVE_BOOT/scratch/core.img \
  --install-modules="linux normal iso9660 biosdisk memdisk search tar ls" \
  --modules="linux normal iso9660 biosdisk search" \
  --locales="" \
  --fonts="" \
  "boot/grub/grub.cfg=$HOME/LIVE_BOOT/scratch/grub.cfg"

cat \
  /usr/lib/grub/i386-pc/cdboot.img \
  $HOME/LIVE_BOOT/scratch/core.img \
  > $HOME/LIVE_BOOT/scratch/bios.img

xorriso \
  -as mkisofs \
  -iso-level 3 \
  -full-iso9660-filenames \
  -volid "DEBIAN_CUSTOM" \
  -eltorito-boot \
    boot/grub/bios.img \
    -no-emul-boot \
    -boot-load-size 4 \
    -boot-info-table \
    --eltorito-catalog boot/grub/boot.cat \
  --grub2-boot-info \
  --grub2-mbr /usr/lib/grub/i386-pc/boot_hybrid.img \
  -eltorito-alt-boot \
    -e EFI/efiboot.img \
    -no-emul-boot \
  -append_partition 2 0xef ${HOME}/LIVE_BOOT/scratch/efiboot.img \
  -output "${HOME}/LIVE_BOOT/debian-custom.iso" \
  -graft-points \
    "${HOME}/LIVE_BOOT/image" \
    /boot/grub/bios.img=$HOME/LIVE_BOOT/scratch/bios.img \
    /EFI/efiboot.img=$HOME/LIVE_BOOT/scratch/efiboot.img

cp ~/LIVE_BOOT/debian-custom.iso /home/ww/
chown ww:ww /home/ww/debian-custom.iso