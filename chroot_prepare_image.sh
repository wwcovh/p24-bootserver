echo "debian-live" > /etc/hostname

apt-get update

apt-get install --no-install-recommends -y \
  linux-image-amd64 \
  live-boot \
  systemd-sysv

apt-get install --no-install-recommends -y \
  firefox-esr \
  htop \
  iftop \
  iotop \
  iproute2 \
  isc-dhcp-client \
  iputils-ping \
  lightdm \
  lxde \
  nano \
  net-tools \
  screen \
  stress

apt-get clean

echo "root:abcdef" | chpasswd
useradd -m -s /bin/bash test
echo "test:test" | chpasswd