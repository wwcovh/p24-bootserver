#!ipxe
echo Boot menu
menu Main menu
item debian10lb Debian Live Build
item acronis2019 Acronis 2019
item memtest86 Memtest86
item windows-menu Windows >

choose os && goto ${os}

# :something is a 'goto' label.

:debian10lb
kernel http://172.31.200.1/debian-live/vmlinuz
module http://172.31.200.1/debian-live/initrd.img
imgargs vmlinuz boot=live config hooks=filesystem ${conkern} username=live noeject fetch=http://172.31.200.1/debian-live/filesystem.squashfs
boot

:memtest86
sanboot http://172.31.200.1/memtest/Memtest86-4.3.7.iso
boot

:windows-menu
menu Windows
item windows10_1903_cz Windows 10 build 1903 CZ
item
item --key 0x08 back Back to Main menu...

:windows10_1903_cz
sanboot http://172.31.200.1/windows/Windows10_1903_CZ_x86+x64.iso
boot